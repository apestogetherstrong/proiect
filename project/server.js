const express = require('express')
const bodyParser = require('body-parser')
const Sequelize = require('sequelize')
const Op = Sequelize.Op
const cors = require('cors')
const mysql = require('mysql2/promise')

 
let sequelize = new Sequelize('experiences_db', 'app', 'welcome123', {
    dialect : 'mysql'
})
 
let Experience = sequelize.define('experience', {
    user : {
        type : Sequelize.STRING,
        allowNull : true,
        validate : {
            len : [1,20]
        },
        defaultValue: 'test_user'
    },
    mijloc : {
        type : Sequelize.STRING,
        allowNull : false,
        validate : {
            len : [1,20]
        }
    },
    plecare : {
        type : Sequelize.STRING,
        allowNull : false,
        validate : {
            len : [1,20]
        }
    },
    sosire : {
        type : Sequelize.STRING,
        allowNull : false,
        validate : {
            len : [1,20]
        }
    },
    ora_plecare : {
        type : Sequelize.INTEGER,
        allowNull : false,
        validate : {
            isInt : true,
            min: 0,
            max: 23
        }
    },
    ora_sosire : {
        type : Sequelize.INTEGER,
        allowNull : false,
        validate : {
            isInt : true,
            min: 0,
            max: 23
        }
    },
    durata : {
        type : Sequelize.INTEGER,
        allowNull : false,
        validate : {
            isInt : true,
            min : 0
        }
    },
    grad_aglomerare : {
        type : Sequelize.STRING,
        allowNull : false,
        validate : {
            len : [1,20]
        }
    },
    observatii : {
        type : Sequelize.STRING,
        allowNull : false,
        validate : {
            len : [1,200]
        }
    },
    nivel_satisfactie : {
        type : Sequelize.STRING,
        allowNull : false,
        validate : {
            len : [1,20],
            min: 1,
            max: 5
        }
    },
},
{
    tableName: 'experience'
})

let User = sequelize.define("user", {
   username : {
       type : Sequelize.STRING,
       allowNull : false,
       len : [5, 30]
   },
   
   email : {
       type : Sequelize.STRING,
       allowNull : false,
       len : [5, 50],
       isEmail: true
   },
   password : {
       type : Sequelize.STRING,
       allowNull : false,
       len : [5, 30]
   },
   
});

User.hasMany(Experience);
 
 
const app = express()
app.use(cors())
app.use(bodyParser.json())
 
app.post('/sync', async (req, res, next) => {
    try {
        await sequelize.sync({force : true})
        res.status(201).json({message : 'created'})
    } catch (err) {
        next(err)
    }
})
 
 

// app.get('/experiences', async (req, res, next) => {
//     try {
//         const {term} = req.query;
//         let experiences = await Experience.findAll({where: {mijloc: {[Op.like]:'%' + term + '%'}}})  
//         res.status(200).json(experiences)
//         }
//         catch (err) {
//         next(err)
//     }    
// })

app.get('/experiences', async(req, res, next) => {
    try {
        let filter = req.query.filter ? req.query.filter : '';
        let experiences;
        
        if (filter) {
            experiences = await Experience.findAll({
                where : Sequelize.or(
                    {mijloc : {
                        [Op.like] : `%${filter}%`
                    }},
                    {plecare : {
                        [Op.like] : `%${filter}%`
                    }},
                    {sosire : {
                        [Op.like] : `%${filter}%`
                    }})
                
            });
        } else {
            experiences = await Experience.findAll();    
        }
        
        
        res.status(200).json(experiences);
        
    } catch(err) {
        next(err);
    }
});

// app.get('/experiences', async (req, res, next) => {
//     try {
//         let experiences = await Experience.findAll()  
//         res.status(200).json(experiences)
//         }
//         catch (err) {
//         next(err)
//     }    
// })
 
app.post('/experiences', async (req, res, next) => {
    try {
        await Experience.create(req.body)
        res.status(201).json({message : 'created'})
    } catch (err) {
        next(err)
    }        
})
 
app.get('/experiences/:id', async (req, res, next) => {
    try {
        let experience = await Experience.findByPk(req.params.id)
        if (experience){
            res.status(200).json(experience)
        }
        else{
            res.status(404).json({message : 'not found'})
        }
    } catch (err) {
        next(err)
    }
})
 
app.put('/experiences/:id', async (req, res, next) => {
    try {
        let experience = await Experience.findByPk(req.params.id)
        if (experience){
            await experience.update(req.body)
            res.status(202).json({message : 'accepted'})
        }
        else{
            res.status(404).json({message : 'not found'})
        }
    } catch (err) {
        next(err)
    }
})
 
app.delete('/experiences/:id', async (req, res, next) => {
    try {
        let experience = await Experience.findByPk(req.params.id)
        if (experience){
            await experience.destroy()
            res.status(202).json({message : 'accepted'})
        }
        else{
            res.status(404).json({message : 'not found'})
        }
    } catch (err) {
        next(err)
    }
})


app.get('/users', async (req, res, next) => {
    try {
        let users = await User.findAll()  
        res.status(200).json(users)
        }
        catch (err) {
        next(err)
    }    
})
 
app.post('/users', async (req, res, next) => {
    try {
        await User.create(req.body)
        res.status(201).json({message : 'created'})
    } catch (err) {
        next(err)
    }        
})
 
app.get('/users/:id', async (req, res, next) => {
    try {
        let user = await User.findByPk(req.params.id)
        if (user){
            res.status(200).json(user)
        }
        else{
            res.status(404).json({message : 'not found'})
        }
    } catch (err) {
        next(err)
    }
})
 
app.put('/users/:id', async (req, res, next) => {
    try {
        let user = await User.findByPk(req.params.id)
        if (user){
            await user.update(req.body)
            res.status(202).json({message : 'accepted'})
        }
        else{
            res.status(404).json({message : 'not found'})
        }
    } catch (err) {
        next(err)
    }
})

 
app.use((err, req, res, next) => {
    console.warn(err)
    res.status(500).json({message : ':('})
})
 
app.listen(8080)