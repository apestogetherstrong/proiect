import {EventEmitter} from 'fbemitter'

const SERVER = 'http://3.17.208.207:8080'

class ExperienceStore{
    constructor()
    {
        this.experiences = {}
        this.emitter = new EventEmitter()
    }
    
    async getExperiences(){
        try{
            let response = await fetch(`${SERVER}/experiences`)
            let data = await response.json()
            this.experiences = data
            this.emitter.emit('GET_EXPERIENCES_SUCCESS')
        }
        catch(err)
        {
            console.warn(err)
            this.emitter.emit('GET_EXPERIENCES_ERROR')
        }
    }
    
    async getExperiencesFiltered(filter){
        try{
            let response = await fetch(`${SERVER}/experiences?filter=${filter}`)
            let data = await response.json()
            this.experiences = data
            this.emitter.emit('GET_EXPERIENCES_FILTERED_SUCCESS')
        }
        catch(err)
        {
            console.warn(err)
            this.emitter.emit('GET_EXPERIENCES_FILTERED_ERROR')
        }
    }
    
    async addExperience(experience){
        try{
            await fetch(`${SERVER}/experiences`, {
                method: 'post',
                headers: {
                    'Content-Type':'application/json'
                },
                body: JSON.stringify(experience)
            })
            this.getExperiences()
        }
        catch(err)
        {
            console.warn(err)
            this.emitter.emit('ADD_EXPERIENCE_ERROR')
        }
    }
    
    async deleteExperience(id)
    {
        try{
            await fetch(`${SERVER}/experiences/${id}`, {
                method:'delete'
            })
            this.getExperiences()
        }
        catch(err)
        {
            console.warn(err)
            this.emitter.emit('DELETE_EXPERIENCE_ERROR')
        }
    }
    
    async saveExperience(id,experience)
    {
        try{
            await fetch(`${SERVER}/experiences/${id}`, {
                method: 'put',
                headers: {
                    'Content-Type':'application/json'
                },
                body: JSON.stringify(experience)
            })
            this.getExperiences()
        }
        catch(err)
        {
            console.warn(err)
            this.emitter.emit('SAVE_EXPERIENCE_ERROR')
        }
    }
    
}

export default ExperienceStore