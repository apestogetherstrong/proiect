import React,{Component} from 'react';
import { Link } from 'react-router-dom';
import UserStore from '../../store/UserStore'
  import { ToastContainer, toast } from 'react-toastify';
  import 'react-toastify/dist/ReactToastify.css';


import './Signup.css'

class Signup extends Component{

notify = (x) => toast(x);
    constructor(props)
    {
        super(props)
        
        
        this.store = new UserStore()
        this.add =(user)=>{
            try{
              console.log("Trying to add")
              this.store.addUser(user) 
              this.notify("Account created")
            }
            catch(err)
            {
                this.notify(err) 
            }
            
        }
        
        this.state = {
            username: '',
            email: '',
            password: ''
        }
        this.handleChange=(evt)=>{
            console.log("Handling change")
            this.setState({
                [evt.target.name]:evt.target.value
            })
        }
        this.handleClick=(evt)=>{
            console.log("Handling click")
            if(this.state.username=='')
            {
                this.notify("Username can't be null")
            }
            else if(this.state.email=='')
            {
                this.notify("Email can't be null")
            }
            else if(this.state.password=='')
            {
               this.notify("Password can't be null") 
            }
            else
            {
               this.add({
                username:this.state.username,
                email:this.state.email,
                password:this.state.password
            })  
            }
               
            
            
        }
        
    }

    render(){
        return(
            
            <div className="form-modal2">
    
        <div className="form-toggle2">
            <Link to="login"><button id="login-toggle2">log in</button></Link>
            <Link to="signup"><button id="signup-toggle2">sign up</button></Link>
        </div>
    
        <div id="signup-form2">
            <form>
                <input type="email" placeholder="Enter your email" name="email" onChange={this.handleChange}/>
                <input type="text" placeholder="Choose username" name="username" onChange={this.handleChange}/>
                <input type="password" placeholder="Create password" name="password" onChange={this.handleChange}/>
                <input type="button" style={{backgroundColor:"#57b846", color:"white"}} value="Sign Up" onClick={this.handleClick}/>
                <hr/>
            </form>
       </div>
   <ToastContainer />  </div>
            
            )
    }
    
}

export default Signup