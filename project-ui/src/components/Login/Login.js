import React,{Component} from 'react';
import { Link } from 'react-router-dom';
  import { ToastContainer, toast } from 'react-toastify';
  import 'react-toastify/dist/ReactToastify.css';

import './Login.css'

class Login extends Component{

notify = (x) => toast(x);
    constructor(props)
    {
        super(props)
        
        this.state = {
            username: '',
            password: ''
        }
        this.handleChange=(evt)=>{
            console.log("Handling change")
            this.setState({
                [evt.target.name]:evt.target.value
            })
        }
        this.handleClick=(evt)=>{
            console.log("Handling click")
            if(this.state.username=='')
            {
                this.notify("Username can't be null")
            }
            else if(this.state.password=='')
            {
               this.notify("Password can't be null") 
            }
            else
            {
               this.notify("Searching...")
            }
               
            
            
        }
        
    }

    render(){
        return(
            
            <div class="form-modal">
    
        <div class="form-toggle">
            <Link to="login"><button id="login-toggle">log in</button></Link>
            <Link to="signup"><button id="signup-toggle">sign up</button></Link>
        </div>
    
        <div id="login-form">
            <form>
                <input type="text" name="username" placeholder="Enter email or username" onChange={this.handleChange}/>
                <input type="password" name="password"  placeholder="Enter password" onChange={this.handleChange}/>
                <input type="button" style={{backgroundColor:"#57b846", color:"white"}} value="Log In" onClick={this.handleClick}/>
                <hr/>
            </form>
        </div>
     <ToastContainer />  </div>
            
            )
    }
    
}

export default Login