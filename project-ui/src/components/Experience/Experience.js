import React, {Component} from 'react'
import "./Experience.css"
import Card from 'react-bootstrap/Card'
import ListGroup from 'react-bootstrap/ListGroup'
import styled from 'styled-components'

const StyledCard = styled(Card)`
    width: 500px;
    margin-top: 100px;
    margin-bottom: 30px;
    margin-left: 200px;
    margin-right: 200px;
`
class Experience extends Component
{
    constructor(props){
        super(props)
        this.state = {
            isEditing : false,
            user: this.props.item.user,
            mijloc: this.props.item.mijloc,
            plecare: this.props.item.plecare,
            sosire: this.props.item.sosire,
            ora_plecare: this.props.item.ora_plecare,
            ora_sosire: this.props.item.ora_sosire,
            durata: this.props.item.durata,
            grad_aglomerare: this.props.item.grad_aglomerare,
            observatii: this.props.item.observatii,
            nivel_satisfactie: this.props.item.nivel_satisfactie
        }
        this.delete = () =>
        {
            this.props.onDelete(this.props.item.id)
        }
        this.edit = () =>
        {
            this.setState({
                isEditing:true
            })
        }
        this.cancel = () =>
        {
            this.setState({
                isEditing:false
            })
        }
        this.save = ()=>
        {
            this.props.onSave(this.props.item.id,{
                mijloc:this.state.mijloc,
                plecare:this.state.plecare,
                sosire:this.state.sosire,
                ora_plecare:this.state.ora_plecare,
                ora_sosire:this.state.ora_sosire,
                durata:this.state.durata,
                grad_aglomerare:this.state.grad_aglomerare,
                observatii:this.state.observatii,
                nivel_satisfactie:this.state.nivel_satisfactie
            })
            this.setState({
                isEditing:false
            })
        }
        this.handleChange=(evt)=>{
            this.setState({
                [evt.target.name] : evt.target.value
            })
        }
    }
    
    render()
    {
        let {item} = this.props
        return(
        <div className="card col-sm-6 col-md-4 col-lg-3"
                 style={{
                    margin: "auto",
                    marginBottom: "20px"
                 }}>
                <div className="card-body">
                    <Card.Header style={{backgroundColor: "#57b846"}}><b>Experience</b></Card.Header>
                    <ListGroup variant="flush">
                        <ListGroup.Item>User: {item.user}</ListGroup.Item>
                        <ListGroup.Item>Transport: {item.mijloc}</ListGroup.Item>
                        <ListGroup.Item>Departure: {item.plecare}</ListGroup.Item>
                        <ListGroup.Item>Arrival: {item.sosire}</ListGroup.Item>
                        <ListGroup.Item>Departure hour: {item.ora_plecare}</ListGroup.Item>
                        <ListGroup.Item>Arrival hour: {item.ora_sosire}</ListGroup.Item>
                        <ListGroup.Item>Duration: {item.durata}</ListGroup.Item>
                        <ListGroup.Item>Crowding Degree: {item.grad_aglomerare}</ListGroup.Item>
                        <ListGroup.Item>Observations: {item.observatii}</ListGroup.Item>
                        <ListGroup.Item>Rating: {item.nivel_satisfactie}</ListGroup.Item>
                    </ListGroup>
                </div>
            </div>
            
/*        
        return <div className="EXP">
        <h2>Utilizator: {item.user}</h2>
        <h2>Mijloc: {item.mijloc}</h2>
        <h4>Plecare: {item.plecare}</h4>
        <h4>Sosire: {item.sosire}</h4>
        <h4>Ora_plecare: {item.ora_plecare}</h4>
        <h4>Ora Sosire: {item.ora_sosire}</h4>
        <h4>Durata: {item.durata}</h4>
        <h4>Grad Aglomeratie: {item.grad_aglomerare}</h4>
        <h4>Observatii: {item.observatii}</h4>
        <h4>Nivel Satisfactie: {item.nivel_satisfactie}</h4>
        <div>
        </div>
        </div>*/
    )}
}

export default Experience