import React,{Component} from 'react';
import ExperienceStore from '../../store/ExperienceStore'
import '../ADDEXP/ADDEXP.css'
  import { ToastContainer, toast } from 'react-toastify';
  import 'react-toastify/dist/ReactToastify.css';


class ExperienceForm extends Component{
    
    notify = (x) => toast(x);
    constructor(props)
    {
        super(props)
        
        
        this.store = new ExperienceStore()
        this.add =(experience)=>{
            try{
              this.store.addExperience(experience) 
            }
            catch(err)
            {
                this.notify(err) 
            }
            
        }
        
        this.state = {
            mijloc: '',
            plecare: '',
            sosire: '',
            ora_plecare: '',
            ora_sosire: '',
            durata: '',
            grad_aglomerare: '',
            observatii: '',
            nivel_satisfactie: ''
        }
        this.handleChange=(evt)=>{
            this.setState({
                [evt.target.name]:evt.target.value
            })
        }
        this.handleClick=(evt)=>{
            if(this.state.mijloc=='')
            {
                this.notify("Please select means of transport")
            }
            else if(this.state.plecare=='')
            {
                this.notify("Please select departure point")
            }
            else if(this.state.sosire=='')
            {
                this.notify("Please select arrival")
            }
            else if(this.state.ora_plecare =='' || this.state.ora_plecare < 0 || this.state.ora_plecare > 23)
            {
                this.notify("Wrong hour format")
            }
            else if(this.state.ora_sosire =='' || this.state.ora_sosire < 0 || this.state.ora_sosire > 23)
            {
                this.notify("Wrong hour format")
            }
            else if(this.state.ora_sosire-this.state.ora_plecare < 0)
            {
                this.notify("Ride longer than a day doesn't exist")
            }
            else if(this.state.grad_aglomerare=='')
            {
                this.notify("Please select crowding degree")
            }
            else if(this.state.observatii=='')
            {
                this.notify("Observations can't be null")
            }
            else if(this.state.nivel_satisfactie=='' || this.state.nivel_satisfactie < 1 || this.state.nivel_satisfactie > 5)
            {
               this.notify("Rating must be between 1-5...") 
            }
            else{
                this.add({
                user:this.state.user,
                mijloc:this.state.mijloc,
                plecare:this.state.plecare,
                sosire:this.state.sosire,
                ora_plecare:this.state.ora_plecare,
                ora_sosire:this.state.ora_sosire,
                durata:this.state.ora_sosire-this.state.ora_plecare,
                grad_aglomerare:this.state.grad_aglomerare,
                observatii:this.state.observatii,
                nivel_satisfactie:this.state.nivel_satisfactie 
            })
              this.notify("Experience added!")
            }
        }
        
    }
    
    
  render()
  {
    return(
        
        
    <div>
        <div className="centeredTitle">
            <h1>Add Experience</h1>
        </div>
        <div className="inputs">
            <div>
            <select name="mijloc" onChange={this.handleChange}>
            <option value="" disabled selected>Means of transport: </option>
            <option value="Bus">Bus</option>
            <option value="Tram">Tram</option>
            <option value="Metro">Metro</option>
            <option value="Trolley">Trolley</option>
            <option value="Train">Train</option>
            </select>
            </div>
            <br/>
            <div>
            <select name="plecare" onChange={this.handleChange}>
            <option value="" disabled selected>Departure from: </option>
            <option value="station A">Station A</option>
            <option value="station B">Station B</option>
            <option value="station C">Station C</option>
            <option value="station D">Station D</option>
            <option value="station E">Station E</option>
            </select>
                
            </div>
            <br/>
            <div>
            <select name="sosire" onChange={this.handleChange}>
            <option value="" disabled selected>Arrival at: </option>
            <option value="station A">Station A</option>
            <option value="station B">Station B</option>
            <option value="station C">Station C</option>
            <option value="station D">Station D</option>
            <option value="station E">Station E</option>
            </select>
            </div>
            <div>
                <input type="number" placeholder="Time of departure(0-23):" min="0" max="23" name="ora_plecare" onChange={this.handleChange}/>
            </div>
            <div>
                <input type="number" placeholder="Time of arrival(0-23):" min="0" max="23" name="ora_sosire" onChange={this.handleChange}/>
            </div>
            <div>
            <select name="grad_aglomerare" onChange={this.handleChange}>
            <option value="" disabled selected>Crowding degree: </option>
            <option value="Free">Free</option>
            <option value="Medium">Medium</option>
            <option value="Busy">Busy</option>
            <option value="Full">Full</option>
            </select>
            </div>
            <div>
                <input type="text" placeholder="Observations:" name="observatii" onChange={this.handleChange}/>
            </div>
                <input type="number" placeholder="Rating(1-5):" name="nivel_satisfactie" min="1" max="5" onChange={this.handleChange}/>
            <div>
                <input type="button" style={{backgroundColor:"#57b846"}} value="Add" onClick={this.handleClick}/>
                
          <ToastContainer /></div>
            </div>
        <div/>
    </div>)
  }
}

export default ExperienceForm