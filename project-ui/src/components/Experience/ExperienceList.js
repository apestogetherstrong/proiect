import React,{Component} from 'react';
import ExperienceStore from '../../store/ExperienceStore'
import Experience from './Experience'
import '../ADDEXP/ADDEXP.css'

class ExperienceList extends Component{
    
    constructor()
    {
        let searchS = ''
        super()
        this.state = {
            experiences : [],
            selectedExperience:null,
            searchS: '',
            filter : ''
        }
        this.store = new ExperienceStore()
        this.add =(experience)=>{
            this.store.addExperience(experience)
        }
        this.delete = (id) => {
            this.store.deleteExperience(id)
        }
        this.save = (id,experience)=>{
            this.store.saveExperience(id,experience)
        }
        this.select = (experience)=>{
            this.setState({
                selectedExperience:experience
            })
        }
        this.handleChange=(evt)=>{
            this.setState({
                filter: evt.target.value
            })
        }
        
        this.cancel = ()=>{
            this.setState({
                selectedExperience:null
            })
        }
        
         this.handleClick=(evt)=>{
             
              let filter = this.state.filter
              this.store.getExperiencesFiltered(filter)
        this.store.emitter.addListener('GET_EXPERIENCES_FILTERED_SUCCESS', ()=>{
            this.setState({
                experiences: this.store.experiences
            })
            }) 
            
            
        }
    }
    
    componentDidMount()
    {
        console.log("mounted")
        this.store.getExperiences()
        this.store.emitter.addListener('GET_EXPERIENCES_SUCCESS', ()=>{
            this.setState({
                experiences: this.store.experiences
            })
        })
    }
    
  render()
  {
       
            return <div style={{marginTop: '64px'}}>
    <h3 style={{textAlign: 'center'}}> A list of experiences: </h3>
    <div className="search">
            <div>
                <input type="text" placeholder="Bus..." name="filter" onChange={this.handleChange}/>
                <input type="button" style={{backgroundColor:"#57b846"}} value="Search" onClick={this.handleClick}/>
            </div></div>
    {
         this.state.experiences.map((e,i)=> <Experience key={i} item ={e} onDelete={this.delete} onSave={this.save} onSelect={this.select}/>)
    }
    </div> 
   
  }
}


export default ExperienceList;
