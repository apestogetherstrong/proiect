import React from 'react'
import { Link } from 'react-router-dom';

import './Toolbar.css'

export default class Toolbar extends React.Component{
    render() {
        return(
  <header className="toolbar">
    <nav className="toolbar__navigation">
      <div />
      <div className="toolbar__logo">
        <Link to="/explist">Home</Link>
      </div>
      <div className="spacer" />
      <div className="toolbar_navigation-items">
        <ul>
          <li>
            <Link to="/addexp">Add experience</Link>
          </li>
          <li>
            <Link to="/myexp">My experiences</Link>
          </li>
          <li>
            <Link to="/profile">My account</Link>
          </li>
          <li>
            <Link to="/login">Log In</Link>
          </li>
        </ul>
      </div>
    </nav>
  </header>
)

    }
}