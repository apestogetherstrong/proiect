import React,{Component} from 'react';
import ExperienceList from './Experience/ExperienceList'
import Toolbar from './Toolbar/Toolbar'
import Profile from './Profile/Profile'
import Login from './Login/Login'
import Signup from './Login/Signup'
import ExperienceForm from './Experience/ExperienceForm'
import MyExperienceList from './MyExperiences/MyExperienceList'
import {Switch, BrowserRouter as Router, Route } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';


class App extends Component{
  render()
  {
    return (
      <div className="App">
      <Router>
      <div>
      <Toolbar/>
      <Switch>
      <Route exactly component={Profile} path="/profile" />
      <Route exactly component={ExperienceList} path="/explist" />
      <Route exactly component={Login} path="/login" />
      <Route exactly component={Signup} path="/signup" />
      <Route exactly component={ExperienceForm} path="/addexp" onAdd={this.add}/>
      <Route exactly component={MyExperienceList} path="/myexp"/>
      </Switch>
      </div>
      </Router>
    </div>
    )
  }
}


export default App;




