import React, {Component} from 'react'
import InputGroup from 'react-bootstrap/InputGroup'
import Button from 'react-bootstrap/Button'
import ListGroup from 'react-bootstrap/ListGroup'
import Card from 'react-bootstrap/Card'
import styled from 'styled-components'
import FormControl from 'react-bootstrap/FormControl'
  import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

class MyExperience extends Component
{
    notify = (x) => toast(x);
    constructor(props){
        super(props)
        this.state = {
            isEditing : false,
            user: this.props.item.user,
            mijloc: this.props.item.mijloc,
            plecare: this.props.item.plecare,
            sosire: this.props.item.sosire,
            ora_plecare: this.props.item.ora_plecare,
            ora_sosire: this.props.item.ora_sosire,
            durata: this.props.item.durata,
            grad_aglomerare: this.props.item.grad_aglomerare,
            observatii: this.props.item.observatii,
            nivel_satisfactie: this.props.item.nivel_satisfactie
        }
        this.delete = () =>
        {
            this.props.onDelete(this.props.item.id)
            this.notify("Experience deleted!")
        }
        this.edit = () =>
        {
            this.setState({
                isEditing:true
            })
        }
        this.cancel = () =>
        {
            this.setState({
                isEditing:false
            })
        }
        this.save = ()=>
        {
            
            if(this.state.mijloc=='')
    {
        this.notify("Please select means of transport")
    }
    else if(this.state.plecare=='')
    {
        this.notify("Please select departure point")
    }
    else if(this.state.sosire=='')
    {
        this.notify("Please select arrival")
    }
    else if(this.state.ora_plecare =='' || this.state.ora_plecare < 0 || this.state.ora_plecare > 23)
    {
        this.notify("Wrong hour format")
    }
    else if(this.state.ora_sosire =='' || this.state.ora_sosire < 0 || this.state.ora_sosire > 23)
    {
        this.notify("Wrong hour format")
    }
    else if(this.state.ora_sosire-this.state.ora_plecare < 0)
    {
        this.notify("Ride longer than a day doesn't exist")
    }
    else if(this.state.grad_aglomerare=='')
    {
        this.notify("Please select crowding degree")
    }
    else if(this.state.observatii=='')
    {
        this.notify("Observations can't be null")
    }
    else if(this.state.nivel_satisfactie=='' || this.state.nivel_satisfactie < 1 || this.state.nivel_satisfactie > 5)
    {
       this.notify("Rating must be between 1-5...") 
    }
    else{
            
            this.props.onSave(this.props.item.id,{
                mijloc:this.state.mijloc,
                plecare:this.state.plecare,
                sosire:this.state.sosire,
                ora_plecare:this.state.ora_plecare,
                ora_sosire:this.state.ora_sosire,
                durata:this.state.ora_sosire-this.state.ora_plecare,
                grad_aglomerare:this.state.grad_aglomerare,
                observatii:this.state.observatii,
                nivel_satisfactie:this.state.nivel_satisfactie
            })
            this.notify("Experience updated!") 
            this.setState({
                isEditing:false
            })
        }
        this.handleChange=(evt)=>{
            this.setState({
                [evt.target.name] : evt.target.value
            })
        }
        }
    }
    
    render()
    {
        
        let {item} = this.props
        if(this.state.isEditing){
            return(
                <div className="card col-sm-6 col-md-4 col-lg-3"
                 style={{
                    margin: "auto",
                    marginBottom: "20px"
                 }}>
                <div className="card-body">
                    <Card.Header>Experience</Card.Header>
                    <ListGroup variant="flush">
                        <ListGroup.Item>User: {item.user}</ListGroup.Item>
                        <ListGroup.Item>Transport: {item.mijloc}
                            <InputGroup className="mb-3">
                                <InputGroup.Prepend>
                                  <InputGroup.Text>1</InputGroup.Text>
                                </InputGroup.Prepend>
                                <select name="mijloc" onChange={this.handleChange}>
            <option value="" disabled selected>Transport: </option>
            <option value="Bus">Bus</option>
            <option value="Tram">Tram</option>
            <option value="Metro">Metro</option>
            <option value="Trolley">Trolley</option>
            <option value="Train">Train</option>
            </select>
                            </InputGroup>
                        </ListGroup.Item>
                        <ListGroup.Item>Departure: {item.plecare}
                            <InputGroup className="mb-3">
                                <InputGroup.Prepend>
                                  <InputGroup.Text>2</InputGroup.Text>
                                </InputGroup.Prepend>
                                <select name="plecare" onChange={this.handleChange}>
            <option value="" disabled selected>Departure from: </option>
            <option value="station A">Station A</option>
            <option value="station B">Station B</option>
            <option value="station C">Station C</option>
            <option value="station D">Station D</option>
            <option value="station E">Station E</option>
            </select>
                            </InputGroup>
                        </ListGroup.Item>
                        <ListGroup.Item>Arrival: {item.sosire}
                            <InputGroup className="mb-3">
                                <InputGroup.Prepend>
                                  <InputGroup.Text>3</InputGroup.Text>
                                </InputGroup.Prepend>
                                <select name="sosire" onChange={this.handleChange}>
            <option value="" disabled selected>Arrival at: </option>
            <option value="station A">Station A</option>
            <option value="station B">Station B</option>
            <option value="station C">Station C</option>
            <option value="station D">Station D</option>
            <option value="station E">Station E</option>
            </select>
                            </InputGroup>
                        </ListGroup.Item>
                        <ListGroup.Item>Departure hour(0-23): {item.ora_plecare}
                            <InputGroup className="mb-3">
                                <InputGroup.Prepend>
                                  <InputGroup.Text>4</InputGroup.Text>
                                </InputGroup.Prepend>
                                <FormControl
                                  placeholder="Ora_plecare noua"
                                  aria-label="ora_plecare"
                                  name="ora_plecare" 
                                  value={this.state.ora_plecare} 
                                  onChange={this.handleChange}
                                />
                            </InputGroup>
                        </ListGroup.Item>
                        <ListGroup.Item>Arrival hour(0-23): {item.ora_sosire}
                            <InputGroup className="mb-3">
                                <InputGroup.Prepend>
                                  <InputGroup.Text>5</InputGroup.Text>
                                </InputGroup.Prepend>
                                <FormControl
                                  placeholder="Ora_sosire noua"
                                  aria-label="ora_sosire"
                                  name="ora_sosire" 
                                  value={this.state.ora_sosire} 
                                  onChange={this.handleChange}
                                />
                            </InputGroup>
                        </ListGroup.Item>
                        <ListGroup.Item>Crowding degree: {item.grad_aglomerare}
                            <InputGroup className="mb-3">
                                <InputGroup.Prepend>
                                  <InputGroup.Text>7</InputGroup.Text>
                                </InputGroup.Prepend>
                                <select name="grad_aglomerare" onChange={this.handleChange}>
            <option value="" disabled selected>Crowding degree: </option>
            <option value="Free">Free</option>
            <option value="Medium">Medium</option>
            <option value="Busy">Busy</option>
            <option value="Full">Full</option>
            </select>
                            </InputGroup>
                        </ListGroup.Item>
                        <ListGroup.Item>Observations: {item.observatii}
                            <InputGroup className="mb-3">
                                <InputGroup.Prepend>
                                  <InputGroup.Text>8</InputGroup.Text>
                                </InputGroup.Prepend>
                                <FormControl
                                  placeholder="Observatii noi"
                                  aria-label="observatii"
                                  name="observatii" 
                                  value={this.state.observatii} 
                                  onChange={this.handleChange}
                                />
                            </InputGroup>
                        </ListGroup.Item>
                        <ListGroup.Item>Rating(1-5): {item.nivel_satisfactie}
                            <InputGroup className="mb-3">
                                <InputGroup.Prepend>
                                  <InputGroup.Text>9</InputGroup.Text>
                                </InputGroup.Prepend>
                                <FormControl
                                  placeholder="Nivel satisfactie nou"
                                  aria-label="nivel_satisfactie"
                                  name="nivel_satisfactie" 
                                  value={this.state.nivel_satisfactie} 
                                  onChange={this.handleChange}
                                />
                            </InputGroup>
                        </ListGroup.Item>
                        <Button onClick={this.save}
                            variant="primary"
                            size="lg"
                            style={{marginLeft: "20px", marginTop: "10px"}}>
                        Save
                        </Button>
                        <Button onClick={this.cancel}
                            variant="primary"
                            size="lg"
                            style={{marginLeft: "20px", marginTop: "10px"}}>
                        Cancel
                        </Button>
                    </ListGroup>
                </div>
            </div>
            )}
        
        else{
            return(
            <div className="card col-sm-6 col-md-4 col-lg-3"
                 style={{
                    margin: "auto",
                    marginBottom: "20px"
                 }}>
                <div className="card-body">
                    <Card.Header style={{backgroundColor: "#57b846"}}><b>Experience</b></Card.Header>
                    <ListGroup variant="flush">
                        <ListGroup.Item>User: {item.user}</ListGroup.Item>
                        <ListGroup.Item>Transport: {item.mijloc}</ListGroup.Item>
                        <ListGroup.Item>Departure: {item.plecare}</ListGroup.Item>
                        <ListGroup.Item>Arrival: {item.sosire}</ListGroup.Item>
                        <ListGroup.Item>Departure hour: {item.ora_plecare}</ListGroup.Item>
                        <ListGroup.Item>Arrival hour: {item.ora_sosire}</ListGroup.Item>
                        <ListGroup.Item>Duration: {item.durata}</ListGroup.Item>
                        <ListGroup.Item>Crowding Degree: {item.grad_aglomerare}</ListGroup.Item>
                        <ListGroup.Item>Observations: {item.observatii}</ListGroup.Item>
                        <ListGroup.Item>Rating: {item.nivel_satisfactie}</ListGroup.Item>
                    </ListGroup>
                    <Button onClick={this.edit}
                            variant="primary"
                            size="lg"
                            style={{float: "left"}}>
                        Edit
                    </Button>
                    <Button onClick={this.delete}
                            variant="primary"
                            size="lg"
                            style={{float: "right"}}>
                        Delete
                    </Button>
                </div>
            <ToastContainer /></div>
        )}
    }
}

export default MyExperience