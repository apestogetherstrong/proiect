import React,{Component} from 'react';
import ExperienceStore from '../../store/ExperienceStore'
import MyExperience from './MyExperience'

class MyExperienceList extends Component{
    
    constructor()
    {
        super()
        this.state = {
            experiences : [],
            selectedExperience:null
        }
        this.store = new ExperienceStore()
        this.add =(experience)=>{
            this.store.addExperience(experience)
        }
        this.delete = (id) => {
            this.store.deleteExperience(id)
        }
        this.save = (id,experience)=>{
            this.store.saveExperience(id,experience)
        }
        this.select = (experience)=>{
            this.setState({
                selectedExperience:experience
            })
        }
        this.cancel = ()=>{
            this.setState({
                selectedExperience:null
            })
        }
    }
    
    componentDidMount()
    {
        console.log("mounted")
        this.store.getExperiences()
        this.store.emitter.addListener('GET_EXPERIENCES_SUCCESS', ()=>{
            this.setState({
                experiences: this.store.experiences
            })
        })
    }
    
  render()
  {
       
            return <div style={{marginTop: '64px'}}>
    <h3 style={{textAlign: "center"}}>A list of experiences: </h3>
    {
         this.state.experiences.map((e,i)=> <MyExperience key={i} item ={e} onDelete={this.delete} onSave={this.save} onSelect={this.select}/>)
    }
    </div> 
   
  }
}


export default MyExperienceList;
